(function (n) {
  function t(t) {
    for (
      var r, s, i = t[0], a = t[1], l = t[2], u = 0, A = [];
      u < i.length;
      u++
    )
      (s = i[u]),
        Object.prototype.hasOwnProperty.call(o, s) && o[s] && A.push(o[s][0]),
        (o[s] = 0);
    for (r in a) Object.prototype.hasOwnProperty.call(a, r) && (n[r] = a[r]);
    f && f(t);
    while (A.length) A.shift()();
    return c.push.apply(c, l || []), e();
  }
  function e() {
    for (var n, t = 0; t < c.length; t++) {
      for (var e = c[t], r = !0, i = 1; i < e.length; i++) {
        var a = e[i];
        0 !== o[a] && (r = !1);
      }
      r && (c.splice(t--, 1), (n = s((s.s = e[0]))));
    }
    return n;
  }
  var r = {},
    o = { app: 0 },
    c = [];
  function s(t) {
    if (r[t]) return r[t].exports;
    var e = (r[t] = { i: t, l: !1, exports: {} });
    return n[t].call(e.exports, e, e.exports, s), (e.l = !0), e.exports;
  }
  (s.m = n),
    (s.c = r),
    (s.d = function (n, t, e) {
      s.o(n, t) || Object.defineProperty(n, t, { enumerable: !0, get: e });
    }),
    (s.r = function (n) {
      "undefined" !== typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(n, "__esModule", { value: !0 });
    }),
    (s.t = function (n, t) {
      if ((1 & t && (n = s(n)), 8 & t)) return n;
      if (4 & t && "object" === typeof n && n && n.__esModule) return n;
      var e = Object.create(null);
      if (
        (s.r(e),
        Object.defineProperty(e, "default", { enumerable: !0, value: n }),
        2 & t && "string" != typeof n)
      )
        for (var r in n)
          s.d(
            e,
            r,
            function (t) {
              return n[t];
            }.bind(null, r)
          );
      return e;
    }),
    (s.n = function (n) {
      var t =
        n && n.__esModule
          ? function () {
              return n["default"];
            }
          : function () {
              return n;
            };
      return s.d(t, "a", t), t;
    }),
    (s.o = function (n, t) {
      return Object.prototype.hasOwnProperty.call(n, t);
    }),
    (s.p = "/");
  var i = (window["webpackJsonp"] = window["webpackJsonp"] || []),
    a = i.push.bind(i);
  (i.push = t), (i = i.slice());
  for (var l = 0; l < i.length; l++) t(i[l]);
  var f = a;
  c.push([0, "chunk-vendors"]), e();
})({
  0: function (n, t, e) {
    n.exports = e("56d7");
  },
  "034f": function (n, t, e) {
    "use strict";
    var r = e("85ec"),
      o = e.n(r);
    o.a;
  },
  "56d7": function (n, t, e) {
    "use strict";
    e.r(t);
    e("e260"), e("e6cf"), e("cca6"), e("a79d");
    var r = e("2b0e"),
      o = function () {
        var n = this,
          t = n.$createElement,
          e = n._self._c || t;
        return e("div", { attrs: { id: "app" } }, [e("Widget")], 1);
      },
      c = [],
      s = function () {
        var n = this,
          t = n.$createElement,
          e = n._self._c || t;
        return e("div", { staticClass: "dwidget" }, [
          e(
            "div",
            { staticClass: "row" },
            n._l(n.currencies, function (t, r) {
              return e(
                "div",
                {
                  staticClass: "col-3",
                  on: {
                    click: function (t) {
                      n.form.currency = r;
                    },
                  },
                },
                [
                  e(
                    "div",
                    {
                      staticClass: "dwidget__currency",
                      class: { "--selected": r == n.form.currency },
                    },
                    [
                      e("div", { staticClass: "dwidget__currency__title" }, [
                        n._v(n._s(r)),
                      ]),
                      e("div", { staticClass: "dwidget__currency__amount" }, [
                        n._v("~ " + n._s(t)),
                      ]),
                    ]
                  ),
                ]
              );
            }),
            0
          ),
          n._m(0),
          e(
            "div",
            { staticClass: "dwidget__amount" },
            [
              n._l(n.amounts, function (t) {
                return e(
                  "button",
                  {
                    staticClass: "btn btn-outline-secondary btn-lg",
                    class: { active: n.form.amount == t },
                    attrs: { type: "button" },
                    on: {
                      click: function (e) {
                        n.form.amount = t;
                      },
                    },
                  },
                  [n._v(" $" + n._s(t) + " ")]
                );
              }),
              e(
                "div",
                {
                  staticClass: "d-inline-block ml-3 align-middle",
                  staticStyle: { width: "140px" },
                },
                [
                  e("div", { staticClass: "input-group input-group-lg" }, [
                    n._m(1),
                    e("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: n.form.amount,
                          expression: "form.amount",
                        },
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "0" },
                      domProps: { value: n.form.amount },
                      on: {
                        input: function (t) {
                          t.target.composing ||
                            n.$set(n.form, "amount", t.target.value);
                        },
                      },
                    }),
                  ]),
                ]
              ),
            ],
            2
          ),
          n.qr
            ? e("div", { staticClass: "dwidget__qr text-center" }, [
                e("img", {
                  staticClass: "rounded mx-auto d-block img-thumbnail",
                  style: n.styleQr,
                  attrs: {
                    src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANQAAADUCAYAAADk3g0YAAAAAXNSR0IArs4c6QAAAERlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAA1KADAAQAAAABAAAA1AAAAAC8ArY6AAANbklEQVR4Ae2d244kxw1EvYb//5cleZ4KB4sKxUZWz6WPnpoKMsg8PURlYcbyr7/++ec//iMBCRwh8N8jLppIQAIfBFwofxAkcJCAC3UQplYScKH8GZDAQQIu1EGYWknAhfJnQAIHCbhQB2FqJQEXyp8BCRwk4EIdhKmVBFwofwYkcJCAC3UQplYScKH8GZDAQQIu1EGYWknAhfJnQAIHCbhQB2FqJYH/rQh+/fq1WlT1p//nW5yf/tQ5LPOpp3rm04/11FnPfOoppj/9Wj31a/2T36rzfK2fT6iWmPkSuCHgQt3AUZJAS8CFaomZL4EbAvM7FL3XOyj9eMemnmLWt/Mxn36pP/Xkl/yTTn/2Z0w/xvR7Wud8jDkP9Tbmedp65vuEIhFjCQwEXKgBnqUSIAEXikSMJTAQOP4OxVnaO2p7R07+9GM+9XV++rd+nCf5MZ/9Usz61K/1S/mr3s7L8679We8TikSMJTAQcKEGeJZKgARcKBIxlsBA4PF3qGG2PypNd2TqvINTT0Okeuqr31rPeXhexuyXdOanOM2T6r+a7hPqq30jzvOtCbhQ3/rrc/ivRsCF+mrfiPN8awI/7h2q/Taefido52E+3zGoM07nSTr7Mb/VOd9Pj31C/fRv2PO9lIAL9VLcNvvpBFyon/4Ne76XEnj8HYp38Jee7kAzzs93iNSC9cynX8pP9ckv6eyf8jkPY/pRX+On/dv5fEK1xMyXwA0BF+oGjpIEWgIuVEvMfAncEDj+DsU7903vl0ich3fuz9YThDQf60+fj/6M03yrnvpR/+zYJ9RnfwP2/1EEXKgf9XV6mM8m4EJ99jdg/x9FYH6H4p39s+nwzs55ks58xqxP5086/Rmv9fRr52c9Y86X/JOe/Kl/tdgn1Ff7RpznWxNwob711+fwX42AC/XVvhHn+dYEfv1zB/5rOUG6E1Nfev2/No3LfsynnuZhPfOT31rPfilO/VJ9Ok+qT/2TP+tTfpqHOv2pr7FPqJWg9RK4EHChLjD8KIGVgAu1ErReAhcCn/57KN5peWdu9cvZPj7Sjzr9qbcx/difevJnPv1YT72tT/lJ5zyMU32an35t/ul6+vmEIhFjCQwEXKgBnqUSIAEXikSMJTAQmH8Pxd7pTpv01o/5jNmPOuN0x6fOesapf+vX+jOfMfuneVnPmH7Ukz/rmb/qnId+1NvYJ1RLzHwJ3BBwoW7gKEmgJeBCtcTMl8ANgfn3UOmOe9P7Q2I989Mdl/Upn/5tnPpRp3+aj/XMp07/03HqT539OS/zqbM+5Sd99WN9in1CJULqEigIuFAFLFMlkAi4UImQugQKAvPvodIdmLOkO2+r0z/Fp+dN/ZLO86Z86jwP/Vad/do49W/9mJ/8k06/NfYJtRK0XgIXAi7UBYYfJbAScKFWgtZL4EJgfoe6eP32I++wTOKdnzpj+rF+1dt+zD8dt+c53b/lm/rzPCk/9W/rU/6q+4RaCVovgQsBF+oCw48SWAm4UCtB6yVwITD/Ld/F61995J2YRe0du81/uj/9OR/19vytH/0ZJ7+kt37p/K3fOh/7tfOx3icUiRhLYCDgQg3wLJUACbhQJGIsgYHA8d9D8U47zPZR2t5pU//kx/o2//R523nW/uyX/D6bD+fj/O18KZ/9GPuEIhFjCQwEXKgBnqUSIAEXikSMJTAQmN+h0p2VOmflnfV0PvulmPMwn/Ot+fRjP/oz/6vrPA9jzk+d56W+1rd+zGfsE4pEjCUwEHChBniWSoAEXCgSMZbAQGB+h2LvdOdlPu/ArKfO+hTTL+WzH+uTnvyf1jnf2q89P/u39Wle+qd89md+68d6xj6hSMRYAgMBF2qAZ6kESMCFIhFjCQwEXv4OxTtruuO2Z6M/69kv5bf1yT/p7Jdi+jGf52P+q3XO18bt/PTneamvsU+olaD1ErgQcKEuMPwogZWAC7UStF4CFwLzOxTvtBfvIx955237sf7IUBcTzvPqfpdR/uhjmrc9X5v/R0NfilI/6pfSj4/p/MxPsU+oREhdAgUBF6qAZaoEEgEXKhFSl0BB4PH/Lh/vqOlOy9nbfNYzpl+a72md863xOm/qT37MZ3/qqZ75bZz803xtP+b7hCIRYwkMBFyoAZ6lEiABF4pEjCUwEHj891Dpzso7b8rnWVlP/XS8zsd6zk89zc/6lJ909qd/0unPfOrJn/mM23rm0y/Ny3zGPqFIxFgCAwEXaoBnqQRIwIUiEWMJDATm30OlO2e6s6bZWZ/6UW/rmX96vuRHnfPwfMxPOvPpzzjlU2/7s54x50n+zKdfqmd+G/uEaomZL4EbAi7UDRwlCbQEXKiWmPkSuCEwv0PRm3fYdGdlPv1SPfPp19a3+ak/9Tb+7HnYP/FN+qvPn/qdntcnVCKuLoGCgAtVwDJVAomAC5UIqUugIDD/LV/qxTtqyj+t8x2g9ef89Gv1tn+bz/na+jX/3Xn4hFp/gqyXwIWAC3WB4UcJrARcqJWg9RK4EPhy71DpHYB39MtZfvuRfqxf9d82Hf4l50lWPA/z6Zfy23r6sz7FaZ7Vv+2/9vMJlYirS6Ag4EIVsEyVQCLgQiVC6hIoCMx/y8c7MO+gjDkb65NOP9ZTpx/jtr71Z78Ut/PQj/Mlv1Vnf/pR53ytTv/kR/+1nn6MfUKRiLEEBgIu1ADPUgmQgAtFIsYSGAjM71C8w/KOOsz2UZr8k77253nYr/VPfsmf9al/8kt68qdOvzRvyqfOfsmf+fRjPXXWp9gnVCKkLoGCgAtVwDJVAomAC5UIqUugIDD/LR/voEXvI6npznt6PvZr/VlPCPRr8+nH+uRPnX4pZj/mJ3/Wp3z6s546Y/q39fTzCUUixhIYCLhQAzxLJUACLhSJGEtgIDC/Q6XevKOm/HSHTX5tPfPpn3Seh/nUGad+zH91nOZLOudN+UmnX4rpx/z2+2I9Y59QJGIsgYGACzXAs1QCJOBCkYixBAYC89/y8Y6a7qTUWc+zJJ1+rGfM/ORPPdW3+fTjvK0f6+m/+rGe/dp49WvryaOdN+X7hEqE1CVQEHChClimSiARcKESIXUJFASO/x6Kd1reWVedZ6Mfdcach/pn+3GeFPM8r54/9W91njedh/6n6+mXYp9QiZC6BAoCLlQBy1QJJAIuVCKkLoGCwPx7qKLXR2q689KPd2jWM0711NuY/dJ8r/Zv5+P87bzMZ/+ksz/rGdOPMf2ot36sT7FPqERIXQIFAReqgGWqBBIBFyoRUpdAQeDl71DpjsvZ2ztv68/8th/z6deeh/XJP+nsT3/qKW77MZ/+1Dlfqyd/6qdjn1Cnier31gRcqLf++j38aQIu1Gmi+r01gePvULzzki719s6c/JLOfsxPOudnPfXkl3T6M2Y9+zOfMeup06/Npx/r6c98xik/6ezf+jOfsU8oEjGWwEDAhRrgWSoBEnChSMRYAgOB4+9Q6Y7azso78epPvzQP+zFmfeu/5qd5OB9j9m/9WE//5Ee99WN+8kv5nL+NfUK1xMyXwA0BF+oGjpIEWgIuVEvMfAncEJj/mxLpzsrezKee7rjUWZ/8mc+Y/q0f6+nPuPVnfYo5D/tRT35Jpz/z2S/lsz7Fqz/rUz/qPqFIxFgCAwEXaoBnqQRIwIUiEWMJDATm30O1d87T+afv4Illmp/zpHz2a/Pbfsk/+SWd/szneRmznjpj+jNmPuO2H+sZ+4QiEWMJDARcqAGepRIgAReKRIwlMBCY36HaO+sw60cp77yM0zwpn/XMT/O3+cmvnSflJ53zp3zOn/Kps54x8zkf86mznvmnY59Qp4nq99YEXKi3/vo9/GkCLtRpovq9NYH5HYr0eIel3sbpDpx0zpPyOV/KX/3ZjzH9qTNu81m/xqf7t37p++L5mN/2o59PKBIxlsBAwIUa4FkqARJwoUjEWAIDgePvUJyFd1TqjNc7LOvZP+mchzHrW53zsD7pzGec6tP8qT7pnCf1Y37rz/rUb/VnP8Y+oUjEWAIDARdqgGepBEjAhSIRYwkMBB5/hxpm+6PS9o7MO3dbz3z68RDUWb/mp3rqjNN8rb76s5682nlSPvu1sU+olpj5Ergh4ELdwFGSQEvAhWqJmS+BGwI/7h3q5qwfEu/gKT/pvJOn/NQ/+VGnX9I5X5vPfvSjnvxbPeVznhTTL+Un3SdUIqQugYKAC1XAMlUCiYALlQipS6Ag8Pg71Ok7Ks9G/3SHZ/2aTz/GnI8643aeVE+9jdf52/OkfvRL52F+8k9+SfcJlQipS6Ag4EIVsEyVQCLgQiVC6hIoCBx/h+KdtZjlj1LZj3dk6mzS5rOeMf2oM2Y+52Wc6qmzPvWjTr82pt86T+vHeVN/5rexT6iWmPkSuCHgQt3AUZJAS8CFaomZL4EbAvP/x+6Nt5IE3o6AT6i3+8o98JMEXKgn6er9dgRcqLf7yj3wkwRcqCfp6v12BFyot/vKPfCTBFyoJ+nq/XYEXKi3+8o98JMEXKgn6er9dgRcqLf7yj3wkwRcqCfp6v12BFyot/vKPfCTBFyoJ+nq/XYEXKi3+8o98JMEXKgn6er9dgRcqLf7yj3wkwT+BuLyAZX8QSSXAAAAAElFTkSuQmCC",
                  },
                }),
                n._m(2),
              ])
            : n._e(),
        ]);
      },
      i = [
        function () {
          var n = this,
            t = n.$createElement,
            e = n._self._c || t;
          return e("div", { staticClass: "text-right" }, [
            e("a", { attrs: { href: "#" } }, [n._v("All currencies")]),
          ]);
        },
        function () {
          var n = this,
            t = n.$createElement,
            e = n._self._c || t;
          return e("div", { staticClass: "input-group-prepend" }, [
            e(
              "span",
              {
                staticClass: "input-group-text",
                attrs: { id: "basic-addon1" },
              },
              [n._v("$")]
            ),
          ]);
        },
        function () {
          var n = this,
            t = n.$createElement,
            e = n._self._c || t;
          return e("p", { staticClass: "mt-2 text-monospace w-50 mx-auto" }, [
            e("small", [n._v("1PdNVV71qNbHxchKiRrxqoR8JohpaLfoMX")]),
            e("br"),
            e("small", [n._v("~")]),
            e("br"),
            e("small", { staticClass: "text-secondary" }, [
              n._v(
                "Scan this QR-code with your wallet to make payment instantly"
              ),
            ]),
          ]);
        },
      ],
      a =
        (e("b680"),
        {
          data: function () {
            return {
              form: { currency: "btc", amount: 0 },
              currencies: { btc: null, eth: null, xrp: null, bch: null },
              amounts: [10, 50, 100, 500],
              rates: { btc: 9254.29, eth: 231.66, xrp: 0.18, bch: 223.96 },
              qr: null,
              clicks: 0,
            };
          },
          computed: {
            styleQr: function () {
              return {
                transform: "rotate(".concat(
                  this.clicks % 2 == 0 ? "90" : "0",
                  "deg)"
                ),
              };
            },
          },
          watch: {
            form: {
              deep: !0,
              handler: function (n) {
                this.setCurrencies(),
                  n.currency && n.amount && (this.qr = !0),
                  this.clicks++;
              },
            },
          },
          methods: {
            setCurrencies: function () {
              if (this.form.amount)
                for (var n in this.currencies)
                  this.currencies[n] = this.rates[n]
                    ? (this.form.amount / this.rates[n]).toFixed(6)
                    : null;
            },
          },
        }),
      l = a,
      f = (e("6cab"), e("2877")),
      u = Object(f["a"])(l, s, i, !1, null, null, null),
      A = u.exports,
      p = { name: "App", components: { Widget: A } },
      d = p,
      v = (e("034f"), Object(f["a"])(d, o, c, !1, null, null, null)),
      m = v.exports,
      g = e("9483");
    Object(g["a"])("".concat("/", "service-worker.js"), {
      ready: function () {
        console.log(
          "App is being served from cache by a service worker.\nFor more details, visit https://goo.gl/AFskqB"
        );
      },
      registered: function () {
        console.log("Service worker has been registered.");
      },
      cached: function () {
        console.log("Content has been cached for offline use.");
      },
      updatefound: function () {
        console.log("New content is downloading.");
      },
      updated: function () {
        console.log("New content is available; please refresh.");
      },
      offline: function () {
        console.log(
          "No internet connection found. App is running in offline mode."
        );
      },
      error: function (n) {
        console.error("Error during service worker registration:", n);
      },
    });
    e("ab8b");
    (r["a"].config.productionTip = !1),
      new r["a"]({
        render: function (n) {
          return n(m);
        },
      }).$mount("#app");
  },
  "6cab": function (n, t, e) {
    "use strict";
    var r = e("73ac"),
      o = e.n(r);
    o.a;
  },
  "73ac": function (n, t, e) {},
  "85ec": function (n, t, e) {},
});
//# sourceMappingURL=app.e17548fa.js.map
