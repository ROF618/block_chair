name = "waybackurls"
type = "ext"

function vertical(ctx, domain)
    print("in waybackurls")
    local cmd = outputdir(ctx) .. "cat " .. domain .. " | waybackurls"

    local data = asset(io.popen(cmd))
    for line in data:lines() do
        newname(ctx, line)
    end
    data:close()
end
