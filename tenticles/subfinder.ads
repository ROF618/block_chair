name = "subfinder"
type = "ext"

function vertical(ctx, domain)
    print("in subfinder")
    local cmd = outputdir(ctx) .. "subfinder -d " .. domain

    local data = asset(io.popen(cmd))
    for line in data:lines() do
        newname(ctx, line)
    end
    data:close()
end
